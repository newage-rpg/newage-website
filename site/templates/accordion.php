<?php namespace ProcessWire;?>
<main id="main" class="uk-container">
  <p><?= $page->textbox ?></p>
	<ul uk-accordion>
	<?php foreach ($page->children as $key => $child): ?>
    <li>
			<a class="uk-accordion-title" href><?= $child->title ?></a>
				<div class="uk-accordion-content">
					<?= $child->textbox ?>
				</div>
    </li>
	<?php endforeach ?>
	</ul>
</main>
