<?php namespace ProcessWire;

// Erstelle alle Variabeln
$mp = 0; // Missions Pins
$lp = 0; // Leiter Pins
$ps = 0; // Punktestand
$gm = 0; // Gastmissionen
$lb = 0; // Logbücher

// Rang -> Ränge sind intern Seiten ID's des zugrundeliegende Repeater Elements
$char_rank = $page->rank; 

// Wird in der Tabelle eingefügt um eine Meldung abzugeben, ob der bestehende Rang okay ist
$rank_report = ''; 

// Wandelt das Datum des Chars in YYYY-MM-DD um
$char_entry = date_create($datetime->date('Y-m-d', $page->date)); 

// Ruft das heutige Datum auf und wandelt es in das selbe Format um
$today = date_create($datetime->date('Y-m-d'));

// Berechnet die Differenz zwischen den beiden Datums
$interval = date_diff($char_entry, $today);

// Wandelt die Differenz in Tage um: '%a'
$char_days = $interval->format('%a');

// Suche die erste Flotte Seite in der Hierarchie über der aktuelle Seite
$rank_group = $page->parent('template=fleet')->rank_group;

// Füre die Suche aus und speicher es in ein PageArray
$ranks = $pages->find("template=rank, parent=$rank_group");

// TODO TESTEN
$char_rank_next = $pages->get("template=rank, parent=$rank_group, title=$char_rank->title");
$char_rank_next = $char_rank_next->next;

if ($page->children('template=char_mission')) {
  foreach ($page->children('template=char_mission') as $child) {
	
	// Funktion stringToNumber() zum Umwandeln von leeren String zu 0
    $mp = $mp + stringToNumber($child->mp);
    $lp = $lp + stringToNumber($child->lp);
    $ps = $ps + stringToNumber($child->ps);
    $gm = $gm + stringToNumber($child->gm);
    $lb = $lb + stringToNumber($child->lb);
  }

foreach ($ranks as $rank) {
	if (
		$mp >= $rank->mp &&
		$lp >= $rank->lp  &&
		$ps >= $rank->ps &&
		$gm >= $rank->gm  &&
		//$rank->lb >= $char_rank->lb &&
		//$char_days >= $rank->days &&
		$char_rank != $rank
   	) {
		$rank_report = 
			"<p class='uk-alert-warning'>
			Beförderungvergleich zum <strong>$rank->title</strong>/Aktueller Stand (MP: <strong>$rank->mp</strong>/$mp, LP: <strong>$rank->lp</strong>/$lp, PS: <strong>$rank->ps</strong>/$ps)
			</p>";
// , GM: $rank->gm/$gm, LB: $rank->lb/$lb, Min: $rank->days/$char_days
	} else {
		$rank_report = '';
	}
	// $rank_report .= "<p>$rank->title --- $rank->mp:$mp -- $rank->lp:$lp</p>";
}

// Generelle Warnung, wenn Beförderungsakte was anderes festlegt als gesetzter Rang
if ($page->child('report_typ=2|3')->rank != $char_rank) {
	$rank_report = '<p class="uk-alert-danger uk-text-large uk-text-center">Rang oder Beförderungsakte nicht in Ordnung</p>';
}

$body = "
  $rank_report
  <table class='uk-table'>
    <thead>
      <tr>
        <th>Missions Pins</th>
        <th>Leiter Pins</th>
        <th>Punktestand</th>
        <th>Gastmissionen</th>
        <!-- <th>Logbücher</th> -->
        <th>Aktiv in Tage</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>$mp</td>
        <td>$lp</td>
        <td>$ps</td>
        <td>$gm</td>
        <!-- <td>$lb</td> -->
        <td>$char_days</td>
      </tr>
    </tbody>
  </table>
  ";
} else {
$body = 'Keine Daten vorhanden';
}
/* 
Wenn was mit dem Baummenü nicht funktioniert, nach </table> folgendes einfügen:

<style>
.ui-resizable {
  position: fixed;
}
</style>


*/


return $body;
?>
