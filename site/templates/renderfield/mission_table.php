<?php namespace ProcessWire;

  $body = "
  <table class='uk-table uk-table-striped'>
    <thead>
      <tr>
        <th>Missionstitel</th>
        <th>Datum</th>
        <th>Anwesenheit</th>
        <th>Missions Pins</th>
        <th>Leiter Pins</th>
        <th>Punktestand</th>
        <th>Gastmissionen</th>
        <th>Missionsnotiz</th>
      </tr>
    </thead>
    <tbody>";
    foreach ($page->children('template=char_mission') as $mission) {
      $missionDate = $datetime->date("d.m.Y", $mission->date);
			$edit_link = $mission->title;
			$missonNote = '';
			if ($user->hasRole('superuser')) {
				$edit_link = "<a href='$mission->editUrl'>$mission->title</a>";
			}
			if ($user->hasRole('koxo-login')) {
				$missonNote = $sanitizer->removeNewlines($mission->textarea, '<br>');
			}
      $body .= "
        <tr>
          <td>$edit_link</td>
          <td>$missionDate</td>
          <td>{$mission->missions_present->title}</td>
          <td>$mission->mp</td>
          <td>$mission->lp</td>
          <td>$mission->ps</td>
          <td>$mission->gm</td>
          <td class='uk-table-shrink'>$missonNote</td>
        </tr>
      ";
    }
  $body .= "
    </tbody>
  </table>
  ";

return $body;
?>
