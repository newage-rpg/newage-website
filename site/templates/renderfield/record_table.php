<?php namespace ProcessWire;

  $body = "
  <table class='uk-table uk-table-striped'>
    <thead>
      <tr>
        <th>Titel</th>
        <th>Datum</th>
        <th>Stardate</th>
        <th>Meldungsart</th>
        <th>ggf. neuer Rang</th>
        <th>Anmerkung</th>
      </tr>
    </thead>
    <tbody>";
    foreach ($page->children('template=char_record') as $record) {
      $recordDate = $datetime->date("d.m.Y", $record->date);
      $body .= "
        <tr>
          <td><a href='$record->editUrl'>$record->title</a></td>
          <td>$recordDate</td>
          <td>$record->stardate</td>
          <td>{$record->report_typ->title}</td>
          <td>{$record->rank->title}</td>
          <td>$record->textarea</td>
        </tr>
      ";
    }
  $body .= "
    </tbody>
  </table>
  ";

return $body;
?>
