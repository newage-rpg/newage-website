<?php namespace ProcessWire;

$form = $forms->render('zweitchar', [ 'mitglied' => $user->id ]);
?>

<main id="main" class="uk-container">
	<style>
		.uk-form-label {
			color: #333;
			font-size: .875rem;
			font-weight: 700;
		}
		.PageArray {
			padding: 0;
			list-style: none;
		}
	</style>
  <p><?= $page->textbox ?></p>
		<?php if($user->newage_rules_accepted == 1): ?>
		<?php echo $form->styles; ?>
		<?php echo $form->scripts; ?><?php echo $form; ?>
		<?php else: ?>
		<a href="/account-login/?profile=1" class="uk-button uk-button-danger uk-align-right">
			Neuer Charakter können erst nach Regelbestätigung im Profil angelegt werden
		</a>
		<?php endif ?>
</main>
