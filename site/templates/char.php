<?php namespace ProcessWire;
include("renderfield/pins_calculatet.php");
// Hole alle Ränge der zugehörige Flotte ein
//
//
// Suche die erste Flotte Seite in der Hirachie über der aktuelle Seite
$rank_group = $page->parent('template=fleet')->rank_group;

// Füre die Suche aus
$ranks = $pages->find("template=rank, parent=$rank_group");

// Hole alle Orden der zugehörige Flotte ein
//
//
// Suche die erste Flotte Seite in der Hirachie über der aktuelle Seite
$orden_group = $page->parent('template=fleet')->orden_group;

// Füre die Suche aus
// $ordens = $pages->find("template=orden, parent=$orden_group");
$ordens = $pages->find("template=orden");
?>

<section id="site-headline" class="headline uk-margin-bottom">
  <div class="uk-container">
    <h1 class="uk-heading-divider"><?= $headline ?></h1>
		<?php if ($page->forum_char_pers_entry != ''): ?>
		<span class="uk-align-right">
			<a href="https://forum.newage-rpg.de/viewforum.php?f=<?= $page->forum_char_pers_entry ?>" target="_blank">
				Persönliche Akte im Forum
			</a>
		</span>
		<?php endif ?>
  </div>
</section>

<main id="main" class="uk-container">
<?php if ($user->hasRole('superuser') || $user->hasRole('staff-department') || in_array("{$page->parent->id}", $user->ship_select->getArray())) : ?>
<h2 class="uk-heading-divider">Charakter Pins</h2>
<ul uk-accordion="multiple: true">
    <li class="uk-open">
			<a class="uk-accordion-title uk-background-muted" href="#">Zusammengefasst</a>
			<div class="uk-accordion-content">
				<?= $body // Output from renderfield/pins_calculate ?>
			</div> 
		</li>
    <li class="">
			<a class="uk-accordion-title uk-background-muted" href="#">Missions Tabelle</a>
			<div class="uk-accordion-content">
				<?php include("renderfield/mission_table.php"); ?>
				<?= $body // Output from renderfield/mission_table ?>
			</div>
		</li>
	</ul>
<?php endif ?>
<section>
  <h2 class="uk-heading-divider">Dienstakte</h2>
  <?php foreach ($page->children('template=char_record') as $record) :?>
    <div class="uk-card uk-card-default uk-margin">
      <div class="uk-card-header">
      <?php 
        $label = '';
				// Auszeichnung
        if ($record->report_typ->id == 1) {
          $label= 'uk-label-success';
				}
				// Beförderung
        if ($record->report_typ->id == 2) {
          $label= 'uk-label-success';
				}
				// Degradierung
        if ($record->report_typ->id == 3) {
          $label= 'uk-label-danger';
				}
				// Reserve
        if ($record->report_typ->id == 4) {
          $label= 'uk-label-warning';
				}
				// Versetzung
        if ($record->report_typ->id == 5) {
          $label= '';
				}
				// Lebenslauf
        if ($record->report_typ->id == 6) {
          $label= '';
				}
				// Ernennung
        if ($record->report_typ->id == 7) {
          $label= 'uk-label-success';
        }
				// Titel
        if ($record->report_typ->id == 8) {
          $label= 'uk-label-success';
        }
				// Ausbildungsnachweise
        if ($record->report_typ->id == 9) {
          $label= '';
        }
				// Verweis
        if ($record->report_typ->id == 10) {
          $label= 'uk-label-danger';
        }
      ?>
        <div class="uk-card-badge uk-label <?= $label ?>"><?= $record->report_typ->title ?></div>
        <span class="uk-text-meta"><time><?= $record->stardate ?> (<?= $record->date ?>)</time></span>
        <h3 class="uk-card-title uk-margin-remove-top"><?= $record->title ?></h3>
      </div>
      <?php 
      $message = $record->textbox;
      if ($record->report_typ->id == 1) {
          if (count($ordens->get("title={$record->orden->title}")->images) >= 1) {
            $img = $ordens->get("title={$record->orden->title}")->images->eq(0)->width(300)->url;
          }
					$message = "<img src='$img' style='float:left;padding-right:1rem'>" . $record->textbox;
      }
        ?>
      <div class="uk-card-body">
        <?= $message ?>
      </div>
      <?php if ($user->hasRole('superuser') || $user->hasRole('staff-department')) : ?>
      <div class="uk-card-footer">
       <p><?= $record->textarea ?></p>
      </div>
      <?php endif ?>
      
    </div>
  <?php endforeach ?>
</section>
</main>

