<?php namespace ProcessWire;?>
<main id="main" class="uk-container">
<section class="uk-margin">
	<div class="uk-card uk-card-default">
		<div class="uk-card-body">
			<?= $page->textbox ?>
		</div>
	</div>
</section>
<?php foreach($page->children as $groups): ?>
<section class="headline uk-margin-bottom">
  <div class="uk-container">
		<h2 class="uk-heading-divider"><?= $groups->title ?></h2>
		<table class="uk-table uk-table-hover uk-table-striped">
			<thead>
				<tr>
					<th class="uk-text-center">Orden</th>
					<th class="uk-text-center">Ribbon / Bezeichnung</th>
					<th>Voraussetzungen</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($groups->children as $orden): 
				$o_img = '';
				$b_img = '';
				if($orden->images->eq(1)) {
					$o_img = "<img src='{$orden->images->eq(1)->height(200)->url}'>";
				}
				if($orden->images->eq(0)) {
					$b_img = "<img src='{$orden->images->eq(0)->width(250)->url}'>";
				}
				?>
				<tr>
					<td class="uk-text-center"><?= $o_img ?></td>
					<td class="uk-text-center">
						<?= $b_img ?> 
						<p><?= $orden->title ?></p>
					</td>
					<td><?= $orden->textarea ?></td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
  </div>
</section>
<?php endforeach ?>
</main>
