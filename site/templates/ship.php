<?php namespace ProcessWire;

// we are only using 1 URL segment, so send a 404 if there's more than 1
if(strlen($input->urlSegment2)) throw new Wire404Exception();

// a halt function with logging
function userNoAccess() {
	echo "<h1>Kein Zugriff</h1>";
	// Log it
	wire()->error("Nicht gestatter Zugriff", true);
	// stop everthing
	return exit();
};

// We control on every segment input the roles in ProcessWire, also again later
// for the frontend code. Better too much as too less ;-)


if($input->urlSegment1 == 'pins') {
	if ($user->hasRole('superuser') || $user->hasRole('staff-department') || in_array("$page->id", $user->ship_select->getArray())) {
		// Collect the guest players
		$guests = $pages->find("template=char, has_parent=$page->rootParent, sort=title");
	} else {
		userNoAccess();
	};
};


if($input->urlSegment1 == 'confirm') {
	// If the formular fields are ommited
	if ($input->post->mdate == '' ) {
		userNoAccess();
	};

	if ($user->hasRole('superuser') || $user->hasRole('staff-department') || in_array("$page->id", $user->ship_select->getArray())) {
		$page->setOutputFormatting(false);
    $inputDate = $input->post('mdate');
    $newDate = date("d.m.Y", strtotime($inputDate));
    $mission_titel = $sanitizer->text($newDate);
    if ($input->post->mtitle != '') {
      $mission_titel = $mission_titel . ' - ' . $input->post->text('mtitle');
    }
    if ($input->post->mausfall == 'ausfall') {
      $mission_titel = $mission_titel . ' - ausgefallen';
    }
    $mission_pageName = $sanitizer->pageName($mission_titel, true);

		// collect guest seats
		$guestSeats = new PageArray();
		if ($input->post('guest1') != '') {
			$guestSeats->add($input->post('guest1'));
		}
		if ($input->post('guest2') != '') {
			$guestSeats->add($input->post('guest2'));
		}
		if ($input->post('guest3') != '') {
			$guestSeats->add($input->post('guest3'));
		}
		
	} else {
		userNoAccess();
	};
};

if($input->urlSegment1 == 'imagedb') {
// Nach dem Update auf der Live Seite wieder entfernen. Zum Einmaligen neuerstellen der Vorscha Bilder
// $img->removeVariations();

	if ($user->hasRole('superuser') || $user->hasRole('staff-department') || in_array("$page->id", $user->ship_select->getArray())) {

		// Wir prüfen den Eingang vom Sende-Button 'imgup', nicht das File selbst
		// da das nie funktionierte
		if ($input->post->imgup) {
			$p = $page;
			$p->setOutputFormatting(false);

			$u = new WireUpload('img_front');
			$u->setMaxFiles(1);
			$u->setOverwrite(true);
			$u->setValidExtensions(array('jpg', 'jpeg', 'png'));
			$u->setDestinationPath($p->images_frontend->path());
			// WireUpload->execute gibt immer ein Array aus, daher ein foreach
			foreach($u->execute() as $filen) {
				$p->images_frontend->add($filen);
				// Wir wollen nach dem speichern, gleich das Pageimage Objekt haben
				$pageimage = $p->images_frontend->getFile($filen);
				// Erzeuge immer die Varainten neu, falls es das Bild schon mal gab
				$pageimage->rebuildVariations();
			}
			$p->save();
		}

	} else {
		userNoAccess();
	};
};
?>

<section id="site-headline" class="headline uk-margin-bottom">
  <div class="uk-container">
    <h1 class="uk-heading-divider"><?= $headline ?></h1>
		<ul class="uk-subnav uk-subnav-divider uk-subnav-pill">
			<li><a href="<?= $page->url ?>">Einheit Info</a></li>
			<?php if ($user->hasRole('superuser') || $user->hasRole('staff-department') || in_array("$page->id", $user->ship_select->getArray())): ?>
			<li><a href="<?= $page->url ?>pins/">Punkte</a></li>
			<li>
				<a href>Mehr<span uk-icon="icon: triangle-down"></span></a>
				<div uk-dropdown="mode: click">
						<ul class="uk-nav uk-dropdown-nav">
						<li><a href="<?= $page->url ?>imagedb/">Bilder-DB</a></li>
						<!-- <li class="uk-disabled"><a href="#"><span style="text-decoration: line-through;">Tolle neue Funktion</span></a></li> -->
						<!-- <li class="uk-disabled"><a href="#"><span style="text-decoration: line-through;">Tolle neue Funktion</span></a></li> -->
						</ul>
				</div>
			</li>
			<?php endif ?>
		</ul>
  </div>
</section>

<main id="main" class="uk-container">

<?php if($input->urlSegment1 == '') : ?>

<section>
  <div class="uk-card uk-card-default uk-margin">
    <div class="uk-card-header">
      <div class="uk-card-badge uk-label">Im Dienst</div>
      <?php if (count($page->images)) : ?>
      <div class="uk-text-center">
				<figure>
					<img src="<?= $page->images->eq(0)->height(300)->url ?>" 
						alt="<?= $page->images->eq(0)->description ?>">
					<?php if ($page->images->eq(0)->description) : ?>
					<figcaption class="uk-text-meta uk-padding-small"><?= $page->images->eq(0)->description ?></figcaption>
					<?php endif ?>
				</figure>
			</div>
      <?php endif ?>
    </div>
    <div class="uk-card-body">
      <p><?= $page->textbox ?></p>
    </div>
  </div>
</section>
<section>
  <table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
    <caption></caption>
    <thead>
      <tr>
				<th>Avatar</th>
        <th>Rang</th>
        <th>Name</th>
        <th>Position</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($page->children as $char) :?>
      <tr>
				<td><?php if (!empty($char->images->eq(0)->url)) { echo "<img src='{$char->images->eq(0)->size(100,100)->url}' alt='{$char->images->eq(0)->description}'>";}?></td>
        <td class="uk-table-shrink"><?= $char->rank->text ?></td>
        <td class="uk-table-expand"><a href="<?= $char->url ?>"><?= $char->title ?></a></td>
				<td><?php if (!empty($char->position->title)) {echo "{$char->position->title} ({$char->position->text})";} ?></td>
      </tr>
      <?php endforeach ?>
    </tbody>
  </table>
</section>

<?php endif; ?>

<?php if($input->urlSegment1 == 'pins') : ?>

<section>
	<form method="post" action="<?= $page->url ?>confirm/">
    <table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
      <caption></caption>
      <thead>
        <tr>
          <th>Avatar</th>
          <th>Rang</th>
          <th>Name</th>
          <th>Anwesend</th>
          <th class="uk-table-shrink">Leiter Pins</th>
          <th>Punkte</th>
					<th>Notiz</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($page->children as $char) :?>
        <tr>
					<td class="uk-table-shrink"><?php if (!empty($char->images->eq(0)->url)) { echo "<img src='{$char->images->eq(0)->size(50,50)->url}' alt='{$char->images->eq(0)->description}'>";}?></td>
          <td class="uk-table-shrink"><?= $char->rank->text ?></td>
          <td class="uk-table-expand"><a href="<?= $char->url ?>"><?= $char->title ?></a></td>
          <td class="uk-table-expand">
            <select class="uk-select" id="<?= $char->id ?>-teil" name="<?= $char->id ?>-teil" required>
              <option value="1" selected>Anwesend</option>
              <option value="2">Entschuldigt</option>
              <option value="3">Unentschuldigt</option>
            </select>
          </td>
          <td><input class="uk-input uk-form-width-small" type="number" step="any" value="0" id="<?= $char->id ?>-lp" name="<?= $char->id ?>-lp"></td>
          <td><input class="uk-input uk-form-width-small" type="number" value="0" max="15" id="<?= $char->id ?>-ps" name="<?= $char->id ?>-ps"></td>
					<td><textarea class="uk-textarea" id="<?= $char->id ?>-note" name="<?= $char->id ?>-note"></textarea></td>
        </tr>
        <?php endforeach ?>
      </tbody>
    </table>
    <hr>
    <p>
      <label><input class="uk-checkbox" type="checkbox" value="ausfall" id="mausfall" name="mausfall"> <span class="uk-label uk-label-warning">Mission ausgefallen?</span> </label>
    </p>
		<hr>
		<ul uk-accordion>
			<li class="-uk-open"> 
				<a class="uk-accordion-title uk-text-primary" href="#">Gäste hinzufügen</a>
				<div class="uk-accordion-content">
					<table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
						<thead>
							<tr>
								<th>Gast Spieler</th>
								<th class="uk-table-shrink">Gast Punkte</th>
								<th>Notiz</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<select class="uk-select" id="guest1" name="guest1">
										<option value=""></option>
										<?php foreach($guests as $gchar):?>
										<option value="<?= $gchar->id ?>"><?= $gchar->title ?></option>
										<?php endforeach ?>
									</select>
								</td>
								<td><input class="uk-input uk-form-width-small" type="number" value="0" max="5" id=guest1-gp" name="guest1-gp"></td>
								<td><textarea class="uk-textarea" id="guest1-note" name="guest1-note"></textarea></td>
							</tr>
							<tr>
								<td>
									<select class="uk-select" id="guest2" name="guest2">
										<option value=""></option>
										<?php foreach($guests as $gchar):?>
										<option value="<?= $gchar->id ?>"><?= $gchar->title ?></option>
										<?php endforeach ?>
									</select>
								</td>
								<td><input class="uk-input uk-form-width-small" type="number" value="0" max="5" id="guest2-gp" name="guest2-gp"></td>
								<td><textarea class="uk-textarea" id="guest2-note" name="guest2-note"></textarea></td>
							</tr>
							<tr>
								<td>
									<select class="uk-select" id="guest3" name="guest3">
										<option value=""></option>
										<?php foreach($guests as $gchar):?>
										<option value="<?= $gchar->id ?>"><?= $gchar->title ?></option>
										<?php endforeach ?>
									</select>
								</td>
								<td><input class="uk-input uk-form-width-small" type="number" value="0" max="5" id="guest3-gp" name="guest3-gp"></td>
								<td><textarea class="uk-textarea" id="guest3-note" name="guest3-note"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
			</li>
		</ul>
		<hr>
    <input class="uk-input uk-form-width-medium" type="date" placeholder="30.02.2020" id="mdate" name="mdate" required>
    <input class="uk-input uk-form-width-large" type="text" placeholder="OPTIONAL: Titel der Mission" id="mtitle" name="mtitle">
    <input type="submit" class="uk-button uk-button-default" value="Absenden">
  </form>
  <div class="uk-alert-primary" uk-alert>
    <p>
      Das System baut aus den Eingaben, den passenden System Titel. Damit wird alles einheitlich abgespeichert und kann so leichter wiedergefunden werden.
    </p>
		<p uk-margin>
			<a class="uk-button uk-button-primary" href="#missionname" uk-toggle>
				So speichert das System
			</a>
			<button class="uk-button uk-button-primary" type="button" uk-toggle="target: #pin-help">
				Hilfe bei der Punktevergabe
			</button>

		</p>
		<div id="missionname" class="uk-flex-top" uk-modal>
			<div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
				<button class="uk-modal-close-default" type="button" uk-close></button>
				<h3>So speichert das System die Mission ab:</h3>
				<dl class="uk-description-list uk-description-list-divider">
					<dt style="text-transform: none;"><h4>24.12.2020</h4></dt>
					<dd>Nur das Datum wurde angegeben. Das System setzt Datum und Titel selbständig gleich.</dd>
					<dt style="text-transform: none;"><h4>24.12.2020 - Tag der Dämmerung</h4></dt>
					<dd>Ein Titel wurde angegeben. Das System verbindet beide Varianten mit einem Strich und setzt das Ergebnis als System Name.</dd>
					<dt style="text-transform: none;"><h4>24.12.2020 - Tag der Dämmerung - ausgefallen</h4></dt>
					<dd>Wenn die Mission ausgefallen ist, finden die davorhergehenden Regeln Verwendung. Zusätzlich wird der Vermerk "ausgefallen" hinzugefügt<br>
						<span class="uk-alert-danger">Wird die Mission als ausgefallen markiert, werden <strong>keine</strong> Pins gespeichert. Die Anwesendheit dagegen wird immer festgehalten!</span></dd>
				</dl>
			</div>
		</div>

		<div id="pin-help" uk-offcanvas="flip: true; overlay: false; bg-close: false">
			<div class="uk-offcanvas-bar">
				<button class="uk-offcanvas-close" type="button" uk-close></button>
				<h3>Spieler</h3>
				<ul>
					<li>0 Punkte <br> War da, hat aber gestört</li>
					<li>1-4 Punkte <br> war da, zeigte aber wenig bis keinen Einsatz</li>
					<li>5-7 Punkte <br> hat mitgespielt</li>
					<li>8-10 Punkte <br> hat viel Einsatz gezeigt</li>
					<li>11-15 Punkte <br> hat die Mission mit geformt</li>
				</ul>
				<h3>Spielleiter</h3>
				<p>1 Leitungspunkt + 15 Punkte</p>
				<p>
					Sollte der Spielleiter nicht zufriedenstellend geleitet haben, so 
					kann man von der Maximalpunktzahl abweichen.
				</p>
				<h3>Gäste</h3>
				<ul>
					<li>1 Punkt <br> war da, zeigte aber wenig bis keinen Einsatz</li>
					<li>2 Punkte <br> hat mitgespielt</li>
					<li>3-4 Punkte <br> hat viel Einsatz gezeigt</li>
					<li>5 Punkte <br> hat die Mission mit geformt</li>
				</ul>
			</div>
		</div>
  </div>
</section>
<?php endif ?>

<?php if($input->urlSegment1 == 'confirm'): ?>
<section>
  <h2>Eingetragen Mission: <?= $input->post('mtitle') ?> vom <?= $newDate ?></h2>
  <table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
    <thead>
      <tr>
        <th>Rang</th>
        <th>Name</th>
        <th>Anwesend</th>
        <th>Mission Pins</th>
        <th class="uk-table-shrink">Leiter Pins</th>
        <th>Punkte</th>
        <th>Notiz</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach ($page->children as $char) :?>
          <?php
          $p = new Page();
          $p->setOutputFormatting(false);
          $p->template = 'char_mission';
          $p->parent = $char;
          $p->name = $mission_pageName;
          $p->title = $mission_titel;
          $p->date = $newDate;
          $p->lp = 0; // Da LP's vom type float sind, wird 0 als basis festgelegt.
          $p->setOutputFormatting(true); // Macht das setzen des SelectOptions Feld einfacher
          $p->missions_present = $input->post->int("{$char->id}-teil");
          $teilnahme = '';
          if ($p->missions_present->id == 1) {
            $teilnahme = 'Anwesend';
          } elseif ($p->missions_present->id == 2) {
            $teilnahme = 'Entschuldigt';
          } elseif ($p->missions_present->id == 3) {
            $teilnahme = 'Unentschuldigt';
          }
          if ($input->post->mausfall != 'ausfall') {
            // $p->mp = $input->post->int("{$char->id}-mp");
            // Setzt den Missionspin abhängig von der Auswahl "Anwesend", da es nur ein Missionspin gibt, ansonsten gibt es keinen Punkt
            if ($p->missions_present->id == 1) {
              $p->mp = 1;
            } else {
              $p->mp = 0;
            }
            $p->lp = $input->post->float("{$char->id}-lp");
            $p->ps = $input->post->int("{$char->id}-ps");
            $p->lb = $input->post->int("{$char->id}-lb");
						$p->textarea = $input->post->textarea("{$char->id}-note");
          }
          $p->save();
          $p->setOutputFormatting(false);
					$p->ship_select = $page->id;
          $p->save();
					$missonNote = wire('sanitizer')->removeNewlines($p->textarea, '<br>');
          ?>
          <tr>
            <td class="uk-table-shrink"><?= $char->rank->text ?></td>
            <td class="uk-table-expand"><a href="<?= $char->url ?>"><?= $char->title ?></a></td>
            <td><?= $teilnahme ?></td>
            <td><?= $p->mp ?></td>
            <td><?= $p->lp ?></td>
            <td><?= $p->ps ?></td>
            <td><?= $missonNote ?></td>
          </tr>
        <?php endforeach ?>
    </tbody>
  </table>
	<h3>Gäste</h3>
  <table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
    <thead>
      <tr>
        <th>Rang</th>
        <th>Name</th>
        <th>Gäste Pins</th>
        <th>Punkte</th>
        <th>Notiz</th>
      </tr>
    </thead>
    <tbody>
        <?php foreach ($guestSeats as $key => $gchar) :?>
          <?php
					$guestNr = $key +1;
          $p = new Page();
          $p->setOutputFormatting(false);
          $p->template = 'char_mission';
          $p->parent = $gchar;
          $p->name = $mission_pageName;
          $p->title = $mission_titel;
          $p->date = $newDate;
					$p->ship_select = $page->id;
          $p->lp = 0; // Da LP's vom type float sind, wird 0 als basis festgelegt.
					$p->mp = 0;
					$p->gm = 1;
					$p->ps = $input->post->int("guest$guestNr-gp");
					if ($p->ps > 5) {
						$p->ps = 1;
					}
					$p->textarea = $input->post->textarea("guest$guestNr-note");
					$missonNote = wire('sanitizer')->removeNewlines($p->textarea, '<br>');
          $p->save();
          
          ?>
          <tr>
            <td class="uk-table-shrink"><?= $gchar->rank->text ?></td>
            <td class="uk-table-expand"><a href="<?= $gchar->url ?>"><?= $gchar->title ?></a></td>
            <td><?= $p->gm ?></td>
            <td><?= $p->ps ?></td>
            <td><?= $missonNote ?></td>
          </tr>
        <?php endforeach ?>
    </tbody>
  </table>
  <a class="uk-button uk-button-primary" href="<?= $page->url ?>">Zurück zur Einheit</a>
</section>
<?php endif ?>

<?php if($input->urlSegment1 == 'imagedb') : ?>

<section>
	<h2>Bilder Upload</h2>
	<form method="post" action="<?= $page->url ?>imagedb/" enctype="multipart/form-data">
		<input type="file" id="img_front" name="img_front">	
		<input name="imgup" type="submit" class="uk-button uk-button-default" value="Absenden">
	</form>
	<?php if(count($page->images_frontend)): ?>
	<h2>Bilderdatenbank der Einheit</h2>
	<table class="uk-table uk-table-divider uk-table-middle">
		<tr>
			<th>Bild</th>
			<th>Name (+ Link kopieren)</th>
		</tr>
		<?php foreach($page->images_frontend->sort('basename') as $img): ?>
		<tr>
			<td>
				<img src="<?= $img->size(500,250,['cropping' => false, 'upscaling' => false])->url ?>" loading="lazy">
			</td>
			<td class="copy-link-container">
				<span class="copy-link-name"><?= $img->basename ?></span><button class="uk-icon-button uk-margin-left" type="button" uk-icon="copy" data-image-url="<?= $img->httpUrl ?>"></button>
			</td>
		</tr>
		<?php endforeach ?>
	</table>
	<?php endif ?>
</section>
<?php endif ?>
<script>
	document.querySelectorAll(".copy-link-container").forEach(copyLinkContainer => {
		const spanTag = copyLinkContainer.querySelector(".copy-link-name");
		const text = spanTag.innerHTML;
		const copyButton = copyLinkContainer.querySelector(".uk-icon-button");
		const dataLink = copyButton.getAttribute("data-image-url");

		copyButton.addEventListener("click", () => {
			navigator.clipboard.writeText(dataLink);

			spanTag.innerHTML = "Kompletter Link kopiert!";
			setTimeout(() => spanTag.innerHTML = text, 2000);
		});
	});
	

</script>
</main>
