<?php namespace ProcessWire;?>
<main id="main" class="uk-container">
<section class="uk-margin">
	<div class="uk-card uk-card-default">
		<div class="uk-card-body">
			<?= $page->textbox ?>
		</div>
	</div>
</section>
<section class="headline uk-margin-bottom">
  <div class="uk-container">
    <h2 class="uk-heading-divider">Neuigkeiten</h2>
  </div>
</section>
<section>
  <?php foreach ($pages->find('template=char_record, has_parent!=1213,sort=-date, limit=5') as $record) :?>
    <div class="uk-card uk-card-default uk-margin">
      <div class="uk-card-header">
      <?php 
        $label = '';
        if ($record->report_typ->id == 2) {
          $label= 'uk-label-success';
        }
      ?>
        <div class="uk-card-badge uk-label <?= $label ?>"><?= $record->report_typ->title ?></div>
        <span class="uk-text-meta"><time><?= $record->stardate ?> (<?= $record->date ?>)</time></span>
        <h2 class="uk-card-title uk-margin-remove-top"><?= $record->parent->title ?> - <?= $record->title ?></h2>
      </div>
      <?php 
      // Hole alle Ränge der zugehörige Flotte ein
      //
      //
      // Suche die erste Flotte Seite in der Hirachie über der aktuelle Seite
      $rank_group = $record->parent('template=fleet')->rank_group;

      // Füre die Suche aus
      $ranks = $pages->find("template=rank, parent=$rank_group");

      // Hole alle Orden der zugehörige Flotte ein
      //
      //
      // Suche die erste Flotte Seite in der Hirachie über der aktuelle Seite
      $orden_group = $record->parent('template=fleet')->orden_group;

      // Füre die Suche aus
     //  $ordens = $pages->find("template=orden, parent=$orden_group");
			$ordens = $pages->find("template=orden");
      $message = $record->textbox;
      if ($record->report_typ->id == 1) {
          if (count($ordens->get("title={$record->orden->title}")->images) >= 1) {
            $img = $ordens->get("title={$record->orden->title}")->images->eq(0)->width(300)->url;
          }
					$message = "<img src='$img' style='float:left;padding-right:1rem'>" . $record->textbox;
      }
        ?>
      <div class="uk-card-body">
        <p><?= $message ?></p>
      </div>
      <?php if ($user->hasRole('superuser') || $user->hasRole('staff-department')) : ?>
      <div class="uk-card-footer">
       <p><?= $record->textarea ?></p>
      </div>
      <?php endif ?>
      
    </div>
  <?php endforeach ?>
</section>
</main>
