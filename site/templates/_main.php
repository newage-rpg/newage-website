<?php namespace ProcessWire;?>
<!DOCTYPE html>

<html lang="de" dir="ltr">

<head> 
	<title><?= $title ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="preload" href="<?= $config->urls->templates ?>css/uikit.min.css" as="style">
	<link rel="stylesheet" type="text/css" href="<?= $config->urls->templates ?>css/uikit.min.css" />
</head>
<body>

<header class="head-nav" id="header">
  <nav class="uk-navbar-container" uk-navbar>
		<div class="uk-navbar-left uk-margin-medium-left">
			<a href="#offcanvas-slide" class="uk-button uk-button-default uk-hidden@m" uk-toggle>Menü</a>
			<div id="offcanvas-slide" uk-offcanvas>
				<div class="uk-offcanvas-bar">
					<ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
						<li class="uk-active"><a href="<?= $pages->get(1)->url ?>"><?= $pages->get(1)->title ?></a></li>
						<li class="uk-parent">
							<a href="#">Star Trek - Einheiten</a>
								<ul class="uk-nav-sub">
								<?php foreach ($pages->find('parent=1064,template=fleet, sort=title') as $fleet) : ?>
									<li class="uk-nav-header"><?= $fleet->title ?></li>
										<?php foreach ($fleet->children('sort=title') as $ship) : ?>
											<li><a href="<?= $ship->url ?>"<?php if ($ship->id == $page->id){echo " class='uk-active'";}?>><?= $ship->title ?></a></li>
										<?php endforeach ?>
								<?php endforeach ?>
								</ul>
						</li>
						<li class="uk-parent">
							<a href="#">Stargate - Einheiten</a>
								<ul class="uk-nav-sub">
								<?php foreach ($pages->find('parent=1082,template=fleet, sort=title') as $fleet) : ?>
									<li class="uk-nav-header"><?= $fleet->title ?></li>
										<?php foreach ($fleet->children as $ship) : ?>
											<li>
												<a href="<?= $ship->url ?>"
												<?php if ($ship->id == $page->id){
													echo " class='uk-active'";
												}?>>
													<?= $ship->title ?>
												</a>
											</li>
										<?php endforeach ?>
								<?php endforeach ?>
								</ul>
						</li>
						<li class="uk-parent">
							<a href="#"><?= $pages->get(3770)->title ?></a>
								<ul class="uk-nav-sub">
								<?php foreach ($pages->get(3770)->children as $child) : ?>
									<li>
										<a href="<?= $child->url ?>"
										<?php if ($child->id == $page->id){
											echo " class='uk-active'";
										}?>>
											<?= $child->title ?>
										</a>
									</li>
								<?php endforeach ?>
									<li class="uk-nav-header">Orga</li>
								<?php if(!$user->isLoggedin()) :?>
									<li>
										<a href="/account-login/"><?= $pages->get(2873)->title ?></a>
									</li>
								<?php endif ?>
								<?php if($user->isLoggedin()) :?>
									<li>
										<a href="/account-login/">Dashboard</a>
									</li>
									<li>
										<a href="/account-login/?profile=1">Profil</a>
									</li>
									<li>
										<a href="/account-login/?logout=1">Abmelden</a>
									</li>
								<?php endif ?>
								</ul>
						</li>
					</ul>
				</div>
			</div>
      <ul class="uk-navbar-nav uk-visible@m">
        <li class="uk-active"><a href="<?= $pages->get(1)->url ?>"><?= $pages->get(1)->title ?></a></li>
        <li>
          <a href="#">Star Trek - Einheiten</a>
          <div class="uk-navbar-dropdown uk-width-auto" uk-dropdown="offset: -1">
            <ul class="uk-nav uk-navbar-dropdown-nav">
            <?php foreach ($pages->find('parent=1064,template=fleet, sort=title') as $fleet) : ?>
              <li class="uk-nav-header"><?= $fleet->title ?></li>
                <?php foreach ($fleet->children('sort=title') as $ship) : ?>
                  <li><a href="<?= $ship->url ?>"<?php if ($ship->id == $page->id){echo " class='uk-active'";}?>><?= $ship->title ?></a></li>
                <?php endforeach ?>
            <?php endforeach ?>
            </ul>
          </div>
        </li>
        <li>
          <a href="#">Stargate - Einheiten</a>
          <div class="uk-navbar-dropdown uk-width-auto" uk-dropdown="offset: -1">
            <ul class="uk-nav uk-navbar-dropdown-nav">
            <?php foreach ($pages->find('parent=1082,template=fleet, sort=title') as $fleet) : ?>
              <li class="uk-nav-header"><?= $fleet->title ?></li>
                <?php foreach ($fleet->children as $ship) : ?>
									<li>
										<a href="<?= $ship->url ?>"
										<?php if ($ship->id == $page->id){
											echo " class='uk-active'";
										}?>>
											<?= $ship->title ?>
										</a>
									</li>
                <?php endforeach ?>
            <?php endforeach ?>
            </ul>
          </div>
        </li>
				<li>
					<a href="#"><?= $pages->get(3770)->title ?></a>
          <div class="uk-navbar-dropdown uk-width-auto" uk-dropdown="offset: -1">
            <ul class="uk-nav uk-navbar-dropdown-nav">
            <?php foreach ($pages->get(3770)->children as $child) : ?>
							<li>
								<a href="<?= $child->url ?>"
								<?php if ($child->id == $page->id){
									echo " class='uk-active'";
								}?>>
									<?= $child->title ?>
								</a>
							</li>
            <?php endforeach ?>
              <li class="uk-nav-header">Orga</li>
						<?php if(!$user->isLoggedin()) :?>
							<li>
								<a href="/account-login/"><?= $pages->get(2873)->title ?></a>
							</li>
						<?php endif ?>
						<?php if($user->isLoggedin()) :?>
							<li>
								<a href="/account-login/">Dashboard</a>
							</li>
							<li>
								<a href="/account-login/?profile=1">Profil</a>
							</li>
							<li>
								<a href="/account-login/?logout=1">Abmelden</a>
							</li>
						<?php endif ?>
            </ul>
          </div>
				</li>
      </ul>
    </div>
    <div class="uk-navbar-right uk-margin-medium-right">
      <ul class="uk-navbar-nav">
				<li>
				<?php $irc = $pages->get(3628);  ?>
				<a href="#"><?= $irc->title ?></a>
					
					<div class="uk-navbar-dropdown uk-width-auto" uk-dropdown="offset: -1">
						<ul class="uk-nav uk-navbar-dropdown-nav">
						<?php foreach ($irc->children as $child): ?>
							<li>
							<a href="<?= $child->url ?>"><?= $child->title ?></a>
							</li>
						<?php endforeach ?>
              <li class="uk-nav-header">Discord Bots</li>
						<?php foreach ($pages->get(3675)->children as $child): ?>
							<li>
							<a href="<?= $child->url ?>"><?= $child->title ?></a>
							</li>
						<?php endforeach ?>
						</ul>
					</div>
				</li>
				<li>
					<a href="https://forum.newage-rpg.de/">Zum Forum</a>
				</li>
      </ul>
    </div>
  </nav>
</header>
<section id="site-headline" class="headline uk-margin-bottom">
  <div class="uk-container">
    <h1 class="uk-heading-divider"><?= $headline ?></h1>
  </div>
</section>
<main id="main" class="uk-container">
  <p><?= $page->textbox ?></p>
</main>
<footer class="uk-container uk-flex uk-flex-between uk-margin-top uk-margin-bottom">
	<ul class="uk-nav uk-nav-default uk-flex">
		<li class="uk-nav-header">Links</li>
		<li class="uk-margin-left"><a href="<?= $pages->get(3800)->url ?>"><?= $pages->get(3800)->title ?></a></li>
		<li class="uk-margin-left"><a href="<?= $pages->get(5014)->url ?>"><?= $pages->get(5014)->title ?></a></li>
	</ul>
	<ul class="uk-nav uk-nav-default uk-flex">
		<li class="uk-margin-left"><a href="#header">Nach oben</a></li>
	</ul>
</footer>
<script src="<?= $config->urls->templates ?>js/uikit.min.js"></script>
<script src="<?= $config->urls->templates ?>js/uikit-icons.min.js"></script>
</body>
</html>
