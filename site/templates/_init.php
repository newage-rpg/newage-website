<?php namespace ProcessWire;

$title = "$page->title";

if ($page->template == 'char') {
  $title = $title . " - " . $page->parent->title;
}

if ($page->headline) {
  $headline = $page->headline;
} else {
  $headline = $title;
}
?>
