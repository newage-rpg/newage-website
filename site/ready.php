<?php namespace ProcessWire;
// if($page->template != 'admin') {
//   $wire->addHookAfter('Pageimage::url', function($event) {
//     static $n = 0;
//     if(++$n === 1) $event->return = $event->object->webp()->url();
//     $n--;
//   });
// }
function stringToNumber($x) {
	if ($x ==  '') {
		$x = 0;
	}
	return $x;
}

$wire->addHookAfter('InputfieldPage::getSelectablePages', function($event) {
  if($event->object->hasField == 'rank') {

    // Die aktuelle Seite
    $actual_page = $event->arguments('page');
    
    // Suche die erste Flotte Seite in der Hirachie über der aktuelle Seite
    $rank_group = $actual_page->parent('template=fleet')->rank_group;

    // Füre die Suche aus
    $event->return = $event->pages->find("template=rank, parent=$rank_group");
  }

  if($event->object->hasField == 'position') {

    // Die aktuelle Seite
    $actual_page = $event->arguments('page');
		if($actual_page->parent->id == 5152) {
			$event->return = $event->pages->find("template=position, sort=sort");
		} else {
		// Suche die erste Flotte Seite in der Hirachie über der aktuelle Seite
			$position_group = $actual_page->parent('template=fleet_overview')->title;

			// Füre die Suche aus
			$event->return = $event->pages->find("template=position, parent.title=$position_group, sort=sort");
		}
  }
  // Momentan deaktiviert
  // if($event->object->hasField == 'orden') {

  //   // Die aktuelle Seite
  //   $actual_page_orden = $event->arguments('page');
    
  //   // Suche die erste Flotte Seite in der Hirachie über der aktuelle Seite
  //   $orden_group = $actual_page_orden->parent('template=fleet')->orden_group;

  //   // Füre die Suche aus
  //   $event->return = $event->pages->find("template=orden, parent=$orden_group");
  // }
});


// Hooking after new User - RS-Mitglied a confirmed
$wire->addHookAfter('LoginRegisterPro::createdUser', function($event) {
	$created_user = $event->arguments(0);

	$edit_link = "https://newage-rpg.de/sds/access/users/edit/?id={$created_user->id}";
	if ($created_user->user_sfo == 1) {
		$sfo = "[SFO]";
	} else {
		$sfo = '';
	}
	$card_title = "{$sfo}[Neuer Spieler] {$created_user->title}";

	$dt = date('c');
	$dt_end = date('c', strtotime('+3 days'));

  $data = [
    'idList' => '65da68244a78977916ed680c',
    'key' => 'fb1d7ec34cd443cbb3f3bfec9398f118',
    'token' => 'ATTA32fa1cbb40eca501027ac064df6468e192ee7eca8d1fa9c8c5830a51c863c77d0E3255FE',
    'name' => $card_title,
		'desc' => $edit_link,
		'start' => $dt,
  ];
  $http = new WireHttp();
	$http->setHeader('Accept', 'application/json');
  $response = $http->post('https://api.trello.com/1/cards', $data);

}); 


// Failed Hook Tests
// Hook for reg. form
//$forms->addHookBefore('FormBuilderProcessor::saveForm', function($event) {
//  $entry = $event->arguments(0);
//	if($entry->name == 'anmeldung') {
//		$field = $entry->getChildByName('zum_backend_uebertragen');
//		$field->attr('value', '1');
//	}
//});

// Hook on Form to Page save
//$wire->addHookAfter('FormBuilderProcessor::savePage', function($event) {
//	$form = $event->object;
//	if($form->formName != 'anmeldung') return;
//
//	$entry = $event->arguments(0);
//	if(!$form->$entry["_savePage"]) return;
//
//	$savedPage = wire('pages')->get($entry["_savePage"]);
//	$entry['rm_number_person'] = '';
//	if($entry['franchise'] == 1) {
//		$savedPage->of(false);
//		$savedPage->position = $entry['st_posten'];
//		$savedPage->save();
//	}
//
//	$event->arguments(0, $entry);
//});

// Hooking after new Char reg
$wire->addHookAfter('FormBuilderProcessor::addedEntry', function($event) {
  $processor = $event->object; // FormBuilderProcessor
  if($processor->formName != 'zweitchar') return; // Go on on form zweitchar

	$char = $event->arguments(0);

	$edit_link = "[Zu allen Char-Anträge des Spielers](https://newage-rpg.de/sds/setup/form-builder/listEntries/?id=5&asmSelect0=&cols%5B%5D=id&cols%5B%5D=char_name&cols%5B%5D=franchise&cols%5B%5D=mitglied&cols%5B%5D=created&qf=mitglied&qo=*%3D&qv={$char['mitglied']}&qv_franchise=&qv_born=&qv_sg_branch=&qv_st_posten=&qv_sg_posten=&qv_st_playtime=&qv_welche_rollenspielartmoechtest_du_gerne_mit_diesem_charakter_spielen=&qv_newage_rpg_regeln=&qv_weitere_verlauf=&dateFrom=&dateTo=&sort=-created&partial=0&limit=25&csv_all=1&submit_filter=Search)";

	$member = wire('pages')->get($char['mitglied']);
	
	switch ($char['franchise']) {
		case '1':
			$franchies_name = "Star Trek";
			break;
		
		case '2':
			$franchies_name = "Stargate";
			break;
		
		default:
			$franchies_name = "Star Trek";
			break;
	}

	if ($member->user_sfo == 1) {
		$sfo = "[SFO]";
	} else {
		$sfo = '';
	}

	$card_title = "{$sfo}[$franchies_name] Neuer Char: {$char['char_name']} von Spieler: {$member->title}";

	$dt = date('c');
	$dt_end = date('c', strtotime('+2 days'));

  $data = [
    'idList' => '65957cf59b3bffed10ba28c6',
    'key' => 'fb1d7ec34cd443cbb3f3bfec9398f118',
    'token' => 'ATTA32fa1cbb40eca501027ac064df6468e192ee7eca8d1fa9c8c5830a51c863c77d0E3255FE',
    'name' => $card_title,
		'desc' => $edit_link,
		'start' => $dt,
		'due' => $dt_end,
		'pos' => 'top',
  ];
  $http = new WireHttp();
	$http->setHeader('Accept', 'application/json');
  $response = $http->post('https://api.trello.com/1/cards', $data);

}); 


$wire->addHook('FormBuilderProcessor::savePageReady', function($event) {

  $processor = $event->object; // FormBuilderProcessor
  $form = $processor->getInputfieldsForm(); // InputfieldForm (if you want it)
  $page = $event->arguments(0); // Page about to be created or saved 
  $entry = $event->arguments(1); // associative array of all entry data, if you want it

  if($form->name != 'zweitchar') return; 

	// Switch per Franchise. ST = 1; SG = 2
	switch ($entry['franchise']) {
		case 1:
			$page->parent = 3491;
			// Change the given list to a array and take the first entry of it
			$st_posten_array = explode('|', $entry['st_posten']);
			$page->position = $st_posten_array[0];
			break;
		
		case 2:
			$page->parent = 6559;
			// Change the given list to a array and take the first entry of it
			$sg_posten_array = explode('|', $entry['sg_posten']);
			$page->position = $sg_posten_array[0];
			break;
		
		default:
			# code...
			break;
	}
}); 

$wire->addHook('FormBuilderProcessor::savePageDone', function($event) {
  $processor = $event->object; // FormBuilderProcessor
  $form = $event->arguments(0); // Saved Page
  if($processor->formName != 'zweitchar') return;

	// $customer = wire('pages')->get($form->client_name);
	$char = $form->title;
	$member = $form->member;
	bd($member);
	// Generate a Passwort
	function generatePassword($length = 16) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}
	$char_pw = generatePassword(16);

	// Systemgruppen ID's: 7=kürzlich reg.; 2=Reg. Mitglied;;Benuzerdiefiniert Gruppen: 13=Star Trek; 10=Stargate
	// Wir setzten als Hauptgruppe immer die Benutzerdefinierte Gruppe wie Star Trek oder Stargate

	// Franchies SD-Forum Cadet/Rekruten ID, ST:829 ; SG:830
	// "franchise_sd_forum": 829,

	switch ($form->parent("template=fleet_overview")->id) {
		case 1064: // Star Trek
			$franchise = 13;
			$franchise_sd_forum = 829;
			break;

		case 1082: // Stargate
			$franchise = 10;
			$franchise_sd_forum = 830;
			break;
		
		default:
			$franchise = 13;
			$franchise_sd_forum = 829;
			break;
	}

  $data_form = [
		"franchise" => $franchise,
    "franchise_sd_forum" => $franchise_sd_forum,
    "char_name" => "$char",
    "player_email" => $member->email,
    "player_pw" => $char_pw,
  ];

	$data_encode = json_encode($data_form);
	bd($data_encode);

	if (true) {
		$http = new WireHttp();
		$http->setTimeout(60);
		$http->setHeader('Accept', 'application/json');
		$response = $http->post('https://forum.newage-rpg.de/anmeldung/nhandle.php', $data_encode);
		$response_error = $http->getError();
		$resp_deco = json_decode($response, true);
		bd($resp_deco);
		// $resp_enc = json_encode($resp_deco, JSON_PRETTY_PRINT);

		if ($response_error == '') {
			$message = wireMail();
			$message->subject("Dein neuer Charakter $char")
					 ->to($member->email)
					 ->from('info@newage-rpg.de')
					 ->body("Der Charakter: $char, wurde mit dem Passwort: $char_pw angelegt.")
					 ->bodyHTML("
<h2>Dein neuer Charakter wurde genehmigt</h2>
<p>Damit kannst du jetzt loslegen und deinen neuen Charakter bespielen!</p>
<h3>Benutzername und Passwort im Forum</h3>
<p>Um in unserem Forum deinen Charakter zu spielen und du deine pers&ouml;nliche Akte vervollst&auml;ndigen kannst, benutze die unterstehenden Anmeldedaten f&uuml;r das Forum:</p>
<p>Benutzername im Forum: <strong>$char</strong></p>
<p>Passwort im Forum: <strong>$char_pw</strong></p>
");
			$numSent = $message->send();
		}
	}

	// save the new ID's from the Forum
	$form->of(false);

	$form->forum_char_id = $resp_deco['user_id'];
	$form->forum_char_pers_entry = $resp_deco['forum_id'];

	$form->save();
});

// Failed Hook Tests
// Hook for reg. form
//$forms->addHookBefore('FormBuilderProcessor::saveForm', function($event) {
//  $entry = $event->arguments(0);
//	if($entry->name == 'anmeldung') {
//		$field = $entry->getChildByName('zum_backend_uebertragen');
//		$field->attr('value', '1');
//	}
//});

// Hook on Form to Page save
//$wire->addHookAfter('FormBuilderProcessor::savePage', function($event) {
//	$form = $event->object;
//	if($form->formName != 'anmeldung') return;
//
//	$entry = $event->arguments(0);
//	if(!$form->$entry["_savePage"]) return;
//
//	$savedPage = wire('pages')->get($entry["_savePage"]);
//	$entry['rm_number_person'] = '';
//	if($entry['franchise'] == 1) {
//		$savedPage->of(false);
//		$savedPage->position = $entry['st_posten'];
//		$savedPage->save();
//	}
//
//	$event->arguments(0, $entry);
//});
