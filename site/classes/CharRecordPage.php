<?php namespace ProcessWire;

use RockMigrations\MagicPage;

class CharRecordPage extends Page {

	use MagicPage;

	public function migrate():void {

		/** @var RockMigrations $rm */
		$rm = $this->wire->modules->get('RockMigrations');

		$rm->setTemplateData('char_record', [
			'fields' => [
				'title' => [
					'columnWidth' => 25,
					'placeholder' => 'Beförderung / Versetzung',
				],
				'date' => [
					'columnWidth' => 25,
					'required' => 1,
					'requiredAttr' => 1,
				],
				'stardate' => [
					'columnWidth' => 25,
				],
				'secret_record' => [
					'columnWidth' => 25,
				],
				'report_typ' => [
					'columnWidth' => 33,
				],
				'rank' => [
					'columnWidth' => 33,
					'label' => 'Neuer Rang',
					'showIf' => 'report_typ=2|3',
				],
				'orden' => [
					'columnWidth' => 34,
					'showIf' => 'report_typ=1',
				],
				'transfer_from' => [
					'columnWidth' => 33,
					'showIf' => 'report_typ=5',
				],
				'transfer_to' => [
					'columnWidth' => 33,
					'showIf' => 'report_typ=5',
				],
				'position' => [
					'columnWidth' => 34,
					'label' => 'Neue Position',
					'showIf' => 'report_typ=5||7',
				],
				'textbox' => [
					'label' => 'Eigene Nachrichtmeldung',
					'notes' => 'ACHTUNG: Überschreibt den Standardtext der hinterlegt wurde. Wird sich auch nicht aktualisieren.',
					'rows' => 7,
				],
				'textarea' => [
					'icon' => 'user-secret',
					'label' => 'Interne Anmerkung',
					'themeBorder' => 'card',
					'themeColor' => 'warning',
					'themeInputSize' => 'm',
					'themeOffset' => 's',
				],
				'images' => [
				],
				'file' => [
				],
			]
		]);
	}

	public function init() {

		// Automatische Titelsetzung für Versetzungen und Beförderungen
		//
		// Pages::added
		$this->wire('pages')->addHookAfter('added', function(HookEvent $event) {
			/** @var Page $page */
			$page = $event->arguments(0);

			if($page->template == 'char_record') {
				// Für frisch angelegte Seiten, bei denen die benötigte Felder noch nicht
				// ausgefüllt sind
				$page->setAndSave('title', 'Titel wird beim speichern angepasst');
			}
		});

		// Pages::saveReady
		$this->wire('pages')->addHookAfter('saveReady', function(HookEvent $event) {
			/** @var Page $page */
			$page = $event->arguments(0);
			/** @var Pages $pages */
			$pages = $event->object;

			if($page->template == 'char_record') {
				// Breche ab, falls die Seite im Papierkorb liegt
				if($page->isTrash) return;
				// Wenn das Feld ausgefüllt ist, was den Titel ausmacht
				if($page->report_typ->id != '') {
					if($page->report_typ->id == 1) {
						$page->title = "{$page->report_typ->title} - {$page->orden->title}";
					}	
					if($page->report_typ->id == 2) {
						$page->title = "{$page->report_typ->title} zum {$page->rank->title} [{$page->rank->text}]";
					}	
					if($page->report_typ->id == 3) {
						$page->title = "{$page->report_typ->title} zum {$page->rank->title} [{$page->rank->text}]";
					}	
					if($page->report_typ->id == 4) {
						$page->title = "{$page->report_typ->title} ({$page->getFormatted('date')})";
					}	
					if($page->report_typ->id == 5) {
						$page->title = "{$page->report_typ->title} [{$page->transfer_to->text}]";
					}	
					if($page->report_typ->id == 6) {
						$page->title = "{$page->report_typ->title}";
					}	
					if($page->report_typ->id == 7) {
						$page->title = "{$page->report_typ->title} zum {$page->position->title}";
					}	
					if($page->report_typ->id == 8) {
						$page->title = "Titelvergabe";
					}	
					if($page->report_typ->id == 9) {
						$page->title = "{$page->report_typ->title}";
					}	
					if($page->report_typ->id == 10) {
						$page->title = "{$page->report_typ->title}";
					}	
					// Sanitize den Titel als Seitenname (URL)
					$name = $event->wire()->sanitizer->pageName($page->title, true);
					// Füge eine Zahl hinzu, falls es den Namen schon gibt
					$page->name = $pages->names()->uniquePageName($name, $page);
				}
			}
		});

		// Füge den Text in die Akte hinzu
		$this->wire->addHookAfter('Pages::saved', function(HookEvent $event) {
			if($event->arguments('page')->template == 'char_record') {
				$page = $event->arguments(0);
				$report_typ = $page->report_typ;
				$parent = $page->parent;
				// report_typ: 
				// 1 == Auszeichnungen,
				// 2 == Beförderung,
				// 3 == Degradierung,
				// 4 == Reserve, 
				// 5 == Versetzungen,
				// 7 == Ernennung
				if ($report_typ == '1' && $page->textbox == '') {
					$page->of(false);
					$page->textbox = "
					<p>Im Namen von Starfleet/Stargate Command, verleihe ich Ihnen $parent->title  
					die Auszeichnung: <strong>{$page->orden->title}</strong>.</p>
					<p>Gezeichnet <em>Rear Admiral Emeraude D. Charlesbois</em></p>
					";
					$page->save('textbox');
				}
				if ($report_typ == '2' && $page->textbox == '') {
					$page->of(false);
					$page->textbox = "
					<p>$parent->title, im Namen von Starfleet Command, befördere ich Sie  
					in den Rang <strong>{$page->rank->title}</strong>.</p>
					<p>Gezeichnet <em>Staff Department</em></p>
					";
					$page->save('textbox');
				}
				if ($report_typ == '3' && $page->textbox == '') {
					$page->of(false);
					$page->textbox = "
					<p>$parent->title, im Namen von Starfleet Command, degradiere ich Sie  
					in den Rang <strong>{$page->rank->title}</strong>.</p>
					<p>Gezeichnet <em>Staff Department</em></p>
					";
					$page->save('textbox');
				}
				if ($report_typ == '4' && $page->textbox == '') {
					$page->of(false);
					$page->textbox = "
					$parent->title wurde in die Reserve versetzt.
					";
					$page->save('textbox');
				}
				if ($report_typ == '5' && $page->textbox == '') {
					$page->of(false);
					$page->textbox = "
					$parent->title wurde von der {$page->transfer_from->title} zur 
					<strong>{$page->transfer_to->title}</strong> als 
					<strong>{$page->position->title}</strong> versetzt.
					";
					$page->save('textbox');
				}
				if ($report_typ == '7' && $page->textbox == '') {
					$page->of(false);
					$page->textbox = "
					<p>$parent->title, im Namen von Starfleet Command, ernenne ich Sie  
					zum <strong>XXX</strong>.</p>
					<p>Gezeichnet <em>Rear Admiral Emeraude D. Charlesbois</em></p>
					";
					$page->save('textbox');
				}
			}
		}); 
	}
}
