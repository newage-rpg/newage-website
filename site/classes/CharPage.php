<?php namespace ProcessWire;

use RockMigrations\MagicPage;

class CharPage extends Page {

	use MagicPage;

	public function migrate():void {

		/** @var RockMigrations $rm */
		$rm = $this->wire->modules->get('RockMigrations');

		$rm->createFields([
			'member' => [
				'label' => 'Mitglied',
				'flags' => 0,
				'type' => 'FieldtypePage',
				'derefAsPage' => 1,
				'inputfield' => 'InputfieldSelect',
				'parent_id' => '',
				'labelFieldName' => 'title',
				'collapsed' => 0,
				'tags' => 'char',
				'template_id' => 'user',
				'findPagesSelector' => 'template=user, sort=title',
				'columnWidth' => 100,
				'required' => '',
			],
			'mp' => [
				'label' => 'Missions Pins',
				'flags' => 0,
				'type' => 'FieldtypeInteger',
				'zeroNotEmpty' => 1,
				'inputType' => 'number',
				'size' => 10,
				'tags' => 'char',
				'columnWidth' => 20,
				'defaultValue' => 0,
				'min' => '0',
			],
			'lp' => [
				'label' => 'Leiter Pins',
				'flags' => 0,
				'type' => 'FieldtypeFloat',
				'precision' => 2,
				'zeroNotEmpty' => 1,
				'inputType' => 'number',
				'size' => 10,
				'tags' => 'char',
				'columnWidth' => 20,
				'min' => '0',
			],
			'ps' => [
				'label' => 'Punktestand',
				'flags' => 0,
				'type' => 'FieldtypeInteger',
				'zeroNotEmpty' => 1,
				'inputType' => 'number',
				'size' => 10,
				'tags' => 'char',
				'columnWidth' => 20,
				'defaultValue' => 0,
				'min' => '0',
			],
			'gm' => [
				'label' => 'Gastmission',
				'flags' => 0,
				'type' => 'FieldtypeInteger',
				'zeroNotEmpty' => 1,
				'inputType' => 'number',
				'size' => 10,
				'tags' => 'char',
				'columnWidth' => 20,
				'defaultValue' => 0,
				'min' => '0',
			],
			'forum_char_id' => [
				'label' => 'Forum Char ID',
				'flags' => 0,
				'type' => 'FieldtypeInteger',
				'zeroNotEmpty' => 1,
				'inputType' => 'number',
				'size' => 10,
				'tags' => 'char',
				'columnWidth' => 20,
				'defaultValue' => 0,
				'min' => '0',
				'collapsed' => 5,
			],
			'forum_char_pers_entry' => [
				'label' => 'Forum Char Persönliche Akte',
				'flags' => 0,
				'type' => 'FieldtypeInteger',
				'zeroNotEmpty' => 1,
				'inputType' => 'number',
				'size' => 10,
				'tags' => 'char',
				'columnWidth' => 20,
				'defaultValue' => 0,
				'min' => '0',
				'collapsed' => 5,
			],
		]);
		$rm->createTemplate('char');
		$rm->setTemplateData('char', [
			'fields' => [
				'images' => [
					'columnWidth' => 34,
				],
				'title' => [
					'columnWidth' => 33,
				],
				'member' => [
					'columnWidth' => 33,
				],
				'rank' => [
					'columnWidth' => 34,
				],
				'position' => [
					'columnWidth' => 33,
				],
				'date' => [
					'columnWidth' => 33,
					'label' => 'Eintritts Datum',
				],
				'pins_calculate' => [
				],
				'pin_cat' => [
				],
				'mission_table' => [
				],
				'pin_cat_END' => [
				],
				'record_cat' => [
				],
				'record_table' => [
				],
				'record_cat_END' => [
				],
				'textarea' => [
					'icon' => 'user-secret',
					'label' => 'Interne Anmerkung',
					'themeBorder' => 'card',
					'themeColor' => 'warning',
					'themeInputSize' => 'm',
					'themeOffset' => 's',
				],
				'forum_char_id' => [
					'columnWidth' => 50,
				],
				'forum_char_pers_entry' => [
					'columnWidth' => 50,
				],
			],
		]);
		$rm->createTemplate('char_mission');
		$rm->setTemplateData('char_mission', [
			'fields' => [
				'title' => [
					'columnWidth' => 34,
					'description' => 'Missionstitel oder Datum der Mission',
					'label' => 'Missionstitel',
					'notes' => 'Datum wäre im Format: DD.MM.YYY',
					'placeholder' => '24.12.2022',
				],
				'date' => [
					'columnWidth' => 33,
					'description' => 'Und hier nochmals das Datum, bis wir es automatisiert haben ;-)',
					'required' => 1,
					'requiredAttr' => 1,
				],
				'missions_present' => [
					'columnWidth' => 33,
				],
				'mp' => [
					'columnWidth' => 25,
				],
				'lp' => [
					'columnWidth' => 25,
				],
				'ps' => [
					'columnWidth' => 25,
				],
				'gm' => [
					'columnWidth' => 25,
				],
				'lb' => [
					'columnWidth' => 20,
					'collapsed' => '4',
				],
				'textarea' => [
					'label' => 'Missionsnotiz',
				],
				'ship_select' => [
					'label' => 'Gespielte Einheit',
					// locked in the frontend
					'collapsed' => '6',
				]
			]
		]);
	}
	
}
