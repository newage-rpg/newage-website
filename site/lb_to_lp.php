<?php namespace ProcessWire;
// zum include bsp.: in einer Template Datei
// Wurde eingesetz zum Migration von Logbuch Pins und Leiter Pins
// für die Forum Charaktere

$alphaChars = $pages->find('template=char, has_parent=1065');
$alphaMiss = new PageArray();
foreach ($alphaChars as $chars ) {
	$charMiss = $chars->children('template=char_mission');
	foreach ($charMiss as $Miss) {
		$alphaMiss->add($Miss);
	}
}

foreach ($alphaMiss as $key => $miss) {
    $missLB = 0;
    $missLP = 0;
    if ($miss->lb != '') {
            $missLB =  $miss->lb;
    }
    if ($miss->lp != '') {
            $missLP =  $miss->lp;
    }
    if ($missLP == 0) {
        $neu = $missLB + $missLP;
    }  elseif ($missLP > $missLB) {
        $neu = $miss->lp;
    } else {
     $neu = $missLB;   
    }
	$miss->of(false);
	$miss->lp = $neu;
	$miss->save('lp');
}
?>
