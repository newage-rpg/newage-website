<?php

namespace ProcessWire;

/** @var RockMigrations $rm */
$rm = $this->wire->modules->get('RockMigrations');
// $rm->createField('demo');
// $rm->setTemplateData('home', [
//   'fields' => [
//     'title',
//     'demo',
//   ],
// ]);

$rm->installModule('ConnectPageFields');

$rm->createFields([
	'chars' => [
		'label' => 'Charaktere',
		'flags' => 32,
		'type' => 'FieldtypePage',
		'derefAsPage' => 0,
		'inputfield' => 'InputfieldPageAutocomplete',
		'distinctAutojoin' => true,
		'operator' => '%=',
		'searchFields' => 'title',
		'parent_id' => '',
		'labelFieldName' => 'title',
		'collapsed' => 0,
		'findPagesSelector' => 'template=char, sort=title',
		'tags' => 'user',
		'editRoles' => [
			0 => 'staff-department',
		],
		'viewRoles' => [
			0 => 'koxo',
			1 => 'staff-department',
		],
		'allowUnpub' => '',
		'template_id' => '',
		'template_ids' => '',
		'findPagesSelect' => '',
		'labelFieldFormat' => '',
		'addable' => '',
		'showIf' => '',
		'themeOffset' => '',
		'themeBorder' => '',
		'themeColor' => '',
		'columnWidth' => 100,
		'required' => '',
		'requiredIf' => '',
	],
	'discord_username' => [
		'label' => 'Discord Benutzername',
		'flags' => 0,
		'type' => 'FieldtypeText',
		'textformatters' => [
			0 => 'TextformatterEntities',
		],
		'notes' => 'Nicht den Discord Nickname, sondern der übergreifende Benutzername',
		'collapsed' => 0,
		'minlength' => 0,
		'maxlength' => 40,
		'showCount' => 0,
		'size' => 0,
		'tags' => 'user',
		'inputfieldClass' => '',
		'showIf' => '',
		'themeInputSize' => '',
		'themeInputWidth' => '',
		'themeOffset' => '',
		'themeBorder' => '',
		'themeColor' => '',
		'themeBlank' => '',
		'columnWidth' => 100,
		'required' => '',
		'requiredAttr' => '',
		'requiredIf' => '',
		'stripTags' => '',
		'placeholder' => '',
		'pattern' => '',
	],
	'birthday' => [
		'label' => 'Geburtstag',
		'flags' => 0,
		'type' => 'FieldtypeDatetime',
		'dateOutputFormat' => 'j F Y',
		'dateInputFormat' => 'Y-m-d',
		'inputType' => 'html',
		'htmlType' => 'date',
		'tags' => 'user',
		'collapsed' => 0,
		'dateSelectFormat' => 'yMd',
		'yearFrom' => 1924,
		'yearTo' => 2044,
		'yearLock' => 0,
		'datepicker' => 0,
		'timeInputSelect' => 0,
		'size' => 25,
		'showIf' => '',
		'themeOffset' => '',
		'themeBorder' => '',
		'themeColor' => '',
		'columnWidth' => 100,
		'required' => '',
		'requiredAttr' => '',
		'requiredIf' => '',
		'timeInputFormat' => '',
		'placeholder' => '',
		'yearRange' => '',
		'dateMin' => '',
		'dateMax' => '',
		'timeStep' => '',
		'timeMin' => '',
		'timeMax' => '',
		'defaultToday' => '',
	],
]);

$rm->setTemplateData('user', [
	'fields' => [
    'title' => [
      'columnWidth' => 33,
      'label' => 'Nickname im RS',
      'notes' => 'Bspw. dein Discord Nick - Hier wird nicht der Name deines zukünftigen Namen abgefragt',
    ],
    'email' => [
      'columnWidth' => 33,
      'label' => 'E-Mail Addresse',
    ],
    'chars' => [
      'columnWidth' => 34,
    ],
    'ship_select' => [
      'columnWidth' => 50,
      'editRoles' => [
        0 => 1050,
      ],
      'flagsAdd' => 32,
      'label' => 'KO/XO-Rechte auf Einheit',
      'viewRoles' => [
        0 => 2740,
        1 => 1050,
      ],
    ],
    'roles' => [
      'columnWidth' => 50,
      'description' => '',
      'notes' => 'Für SD = staff-department
			Für KO/XO = koxo',
    ],
    'discord_username' => [
      'columnWidth' => 50,
    ],
    'birthday' => [
      'columnWidth' => 50,
    ],
    'pass' => [
    ],
    'admin_theme' => [
      'collapsed' => '1',
    ],
    'language' => [
      'collapsed' => '1',
    ],
    'csv_files' => [
    ],
    'branch_parent' => [
    ],
	],
]);

