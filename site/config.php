<?php

/**
 * ProcessWire Configuration File
 *
 * Site-specific configuration for ProcessWire.
 *
 * Please see the file /wire/config.php which contains all configuration options you may
 * specify here. Simply copy any of the configuration options from that file and paste
 * them into this file in order to modify them.
 *
 * ProcessWire 
 * Copyright (C) 2017 by Ryan Cramer
 * Licensed under MPL 2.0
 *
 * https://processwire.com
 *
 */

if(!defined("PROCESSWIRE")) die();

/*** SITE CONFIG *************************************************************************/

/**
 * Enable debug mode?
 *
 * Debug mode causes additional info to appear for use during dev and debugging.
 * This is almost always recommended for sites in development. However, you should
 * always have this disabled for live/production sites.
 *
 * @var bool
 *
 */
$config->debug = true;

$config->moduleInstall('download', true);
$config->moduleInstall('upload', true);
$config->contentTypes('webp', 'image/webp');
$config->imageSizerOptions('webpAdd', true); 
$config->useMarkupRegions = true;
$config->defaultAdminTheme = 'AdminThemeUikit';
$config->prependTemplateFile = '_init.php';
$config->appendTemplateFile = '_main.php';
$config->sessionFingerprint = 2;
$config->pageNumUrlPrefix = 'seite_';
$config->livereload = 1;
$config->usePageClasses = true;
/*** INSTALLER CONFIG ********************************************************************/



/**
 * Installer: Database Configuration
 * 
 */

$config->dbHost = 'db';
$config->dbName = 'db';
$config->dbUser = 'db';
$config->dbPass = 'db';
$config->dbPort = '3306';

/**
 * Installer: User Authentication Salt 
 * 
 * This value was randomly generated for your system on 2021/01/02.
 * This should be kept as private as a password and never stored in the database.
 * Must be retained if you migrate your site from one server to another.
 * Do not change this value, or user passwords will no longer work.
 * 
 */
$config->userAuthSalt = '0ba730c13a5b0fdf58bfb28e927b1850db9598b1'; 

/**
 * Installer: Table Salt (General Purpose) 
 * 
 * Use this rather than userAuthSalt when a hashing salt is needed for non user 
 * authentication purposes. Like with userAuthSalt, you should never change 
 * this value or it may break internal system comparisons that use it. 
 * 
 */
$config->tableSalt = 'f5c527c8fcb73cf612b54daff76ecc96df726ebe'; 

/**
 * Installer: File Permission Configuration
 * 
 */
$config->chmodDir = '0755'; // permission for directories created by ProcessWire
$config->chmodFile = '0644'; // permission for files created by ProcessWire 

/**
 * Installer: Time zone setting
 * 
 */
$config->timezone = 'Europe/Berlin';


/**
 * Installer: Unix timestamp of date/time installed
 * 
 * This is used to detect which when certain behaviors must be backwards compatible.
 * Please leave this value as-is.
 * 
 */
$config->installed = 1609624708;


/**
 * Installer: HTTP Hosts Whitelist
 * 
 */
$config->httpHosts = array('newage-rpg.de', 'new-age-sd.ddev.site');

/**
 * Override the config for live env where you don't want to expose
 * the db auth. Also you can set things diffrent. Like debug to
 * false. It is the opposite aproach as:
 * https://processwire.com/talk/topic/18719-maintain-separate-configs-for-livedev-like-a-boss/
 */
$localLive = __DIR__ . "/config-live.php";
if (is_file($localLive)) include $localLive;
